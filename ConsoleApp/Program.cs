﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int secInStep, stepInSection, floor, result;
            Console.WriteLine("Программа для рассчета секунд жизни при подъеме на определенный этаж");

            try
            {
                Console.Write("Введите колличество секунд на ступеньку: ");
                secInStep = int.Parse(Console.ReadLine());
            }
            catch(Exception e)
            {
                Console.WriteLine("Ошибка ввода, значению будетприсвоен 0");
                secInStep = 0;
                Console.WriteLine($"Текст ошибки: {e.ToString()}");
            }

            try
            {
                Console.Write("Введите колличество ступеней в пролете: ");
            stepInSection = int.Parse(Console.ReadLine());

             }
            catch(Exception e)
            {
                Console.WriteLine("Ошибка ввода, значению будетприсвоен 0");
                stepInSection = 0;
                Console.WriteLine($"Текст ошибки: {e.ToString()}");
            }

            try
            {
                Console.Write("Введите кол-во этажей: ");
            floor = int.Parse(Console.ReadLine());

            }
            catch (Exception e)
            {
                Console.WriteLine("Ошибка ввода, значению будетприсвоен 0");
                floor = 0;
                Console.WriteLine($"Текст ошибки: {e.ToString()}");
            }

            result = secInStep * stepInSection * floor * 2;
            Console.WriteLine($"Вы увеличите свою жизнь на {result} секунд");

            Console.ReadKey();
        }
    }
}
